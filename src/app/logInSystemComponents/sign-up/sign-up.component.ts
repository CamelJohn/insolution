import { AuthService } from './../../shared/services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  hide = true;
  gender = 'male';
  genders = ['male', 'female'];

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

  signUp() {

  }

}
