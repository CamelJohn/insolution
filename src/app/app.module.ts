import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignInComponent } from './logInSystemComponents/sign-in/sign-in.component';
import { SignUpComponent } from './logInSystemComponents/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './logInSystemComponents/forgot-password/forgot-password.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AuthService } from './shared/services/auth.service';
import { HeaderComponent } from './nav/header/header.component';
import { SidenavListComponent } from './nav/sidenav-list/sidenav-list.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from 'src/environments/environment';
import { DashboardComponent } from './generalComponents/dashboard/dashboard.component';
import { ErrorPageComponent } from './generalComponents/error-page/error-page.component';
import { WelcomeComponent } from './generalComponents/welcome/welcome.component';
import { ProfileComponent } from './generalComponents/profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    HeaderComponent,
    SidenavListComponent,
    DashboardComponent,
    ErrorPageComponent,
    WelcomeComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    FlexLayoutModule,
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
