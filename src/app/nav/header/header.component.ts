import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  url = true;
  @Output() sidenavToggle = new EventEmitter<void>();
  links = [
    { text: 'in' , ref: '/log-in'},
    { text: 'up' , ref: '/register-user'},
    { text: 'board', ref: '/dashboard' },
    { text: 'logout', ref: '' },
  ];

  constructor(private router: Router) {
    router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {

        if (event.url === '/log-in' || event.url === '/register-user' || event.url === '/forgot-password' || event.url === '/welcome') {

          this.url = false;
        } else {

          this.url = true;
        }
      }
    });
  }

  ngOnInit() {
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

}
