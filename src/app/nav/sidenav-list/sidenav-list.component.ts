import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {

  @Output() closeSidenav = new EventEmitter<void>();
  links = [
    { text: 'main' , ref: '/welcome'},
    { text: 'check' , ref: 'someRef'},
    { text: 'board', ref: '/dashboard' },
    { text: 'file', ref: 'someRef' },
    { text: 'logout', ref: 'someRef' }
  ];

  constructor() { }

  ngOnInit() {
  }

  onClose() {
    this.closeSidenav.emit();
  }
}
