import { SignInComponent } from './logInSystemComponents/sign-in/sign-in.component';
import { Routes, RouterModule } from '@angular/router';
import { SignUpComponent } from './logInSystemComponents/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './logInSystemComponents/forgot-password/forgot-password.component';
import { NgModule } from '@angular/core';
import { SecureInnerPagesGuard } from './shared/gurad/secure-inner-pages.guard';
import { AuthGuard } from './shared/gurad/auth.guard';
import { WelcomeComponent } from './generalComponents/welcome/welcome.component';
import { DashboardComponent } from './generalComponents/dashboard/dashboard.component';
import { ErrorPageComponent } from './generalComponents/error-page/error-page.component';



const appRoutes: Routes = [
    { path: '', redirectTo: '/log-in', pathMatch: 'full' },
    { path: 'welcome', component: WelcomeComponent },
    { path: 'log-in', component: SignInComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'register-user', component: SignUpComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    // { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] }

    { path: 'not-found', component: ErrorPageComponent, data: {message: 'Page not found!'} },
    { path: '**', redirectTo: '/not-found' }
];


@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes, {useHash :true})
        RouterModule.forRoot(appRoutes)

    ],
    exports: [RouterModule]
})

export class AppRoutingModule {}
