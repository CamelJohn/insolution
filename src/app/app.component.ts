import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Insolution';
  
  url = false;
  constructor(private router: Router) {
    router.events.subscribe(event => {

      if (event instanceof NavigationEnd ) {
  
        if (event.url === '/log-in' || event.url === '/register-user' || event.url === '/forgot-password' || event.url === '/welcome') {
          this.url = false;
        } else {
          this.url = true;
        }
      }
    });
  }
}
